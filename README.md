# Data analysis project for Health Promotion Board #

### What is this repository for? ###

This is a repository of data analysis project for Singapore Health Promotion Board.

### How to set up? ###

#### Set up Python ####

Python 3.5.2 is used for this project. [Anaconda](https://www.continuum.io) is suggested to install Python and for package management. Please refer to the [official website](https://docs.continuum.io/anaconda/install) for more infomations about how to install Ananconda.

#### Set up Python packages

This project uses [Anaconda](https://www.continuum.io) for package management. 

Create an conda enviroment with the following command

```shell
conda env create -f environment.yml
```

Please refer to the [Anaconda guide for managing environments](http://conda.pydata.org/docs/using/envs.html) about how to create and activate an environment on different platforms.

#### Install Postgres, PostGIS and pgAdmin

If you are using Mac, I found that [Postgres.app](https://postgresapp.com) is very easy to use.

If you are using other machines. Please refer to offical website of [Postgres](https://www.postgresql.org/download/) and [PostGIS](http://www.postgis.net) for more information. 

Also I'm using [pgAdmin](https://www.pgadmin.org) as the GUI

#### Database setup

Before you creating and editing tables, make sure you enable PostGIS by running:

`CREATE EXTENSION postgis;`

Scripts for creating table are includes under folder script/table

Scripts for inserting data are includes under folder script/data 

