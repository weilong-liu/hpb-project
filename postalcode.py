import pandas as pd
from datetime import datetime
import numpy as np
import matplotlib.pyplot as plt
import requests
from svy21 import SVY21
import json

raw_participant = pd.read_csv('data/processed/nsc1_participant_raw.csv', na_values='Nan', dtype={'PostalCode': str})
postal_codes = raw_participant[raw_participant['PostalCode'].str.len()==6]['PostalCode'].unique()
print(len(postal_codes))

# Retrive postal code data with one map api
def search_postal_code(postal_code):
    url = 'https://developers.onemap.sg//commonapi/search?searchVal={0}&returnGeom=Y&getAddrDetails=N&pageNum=1'
    response = requests.get(url.format(postal_code))
    return response

postal_codes_full = {}
missing_post = []
error_post = []
for postal_code in postal_codes[:2]:
    try:
        response = search_postal_code(postal_code)
        if response.status_code == 200:
            postal_codes_full[postal_code] = response.json()['results']
            print('S')
        else:
            missing_post.append(postal_code)
            print("missing " + postal_code)
    except:
        error_post.append(postal_code)
        print('.')

with open('postal_onemap.json', 'w') as f:
	json.dump(postal_codes_full, f)

