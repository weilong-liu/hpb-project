import geoplotlib
from geoplotlib.utils import read_csv


data = read_csv('data/processed/postalcode_participant_count.csv')
geoplotlib.dot(data)
geoplotlib.show()